#include "list.h"
#include <iostream>
using std::cout;
using std::endl;

list::list() {
	/* set list to be empty: */
	root = 0;
}

list::list(const list& L) 
{
	this->root = 0;
	
	if(!L.root) return;
	
	this->root = new listNode(L.root->data);
		
	listNode *p = this->root; 
	listNode * r = L.root->next;
	
	while(r) 
	{
		p->next = new listNode(r->data);	
		r = r->next;
		p = p->next;	
	}	
	/* TODO: try to write this without the listNode** stuff.
	 * (You will probably need a special case for the first node...) */
}

list& list::operator=(const list& RHS) {
	/* remember: assignment is just like the copy constructor, but you have to:
	 * 1. check for self-assignment
	 * 2. clean up existing object before copying. */
	if (this == &RHS) return *this;
	this->clear();
	/* now just use the copy constructor code... */
	return *this;
}
/* TODO: write another version of the copy constructor that does not
 * blindly deallocate the LHS nodes, but rather re-uses them, and
 * either (a) deletes the extras if present, or (b) allocates new
 * ones if needed. */

//what i think this means is if the same data is in LHS as rhs then we dont have to delete it
list::~list() {
	this->clear();
}

void list::insertAtFront(int x) {
	this->root = new listNode(x,this->root);
}

bool list::eraseOne(int x) {
	listNode* target = this->root;
	listNode* guyBefore = 0;
	/* now do the search for x: */
	while (target && target->data != x) {
		guyBefore = target;
		target = target->next; /* highly analogous to i++ */
	}
	if (!target) return false; /* x was not found */
	/* unfortunately we need a special case for removing
	 * the first node: */
	if (!guyBefore)
		this->root = target->next;
	else
		guyBefore->next = target->next;
	delete target;
	return true;
}

void list::clear() {
	listNode* doomed = this->root;
	while(this->root) {
		this->root = this->root->next;
		delete doomed;
		doomed = this->root;
	}
}
void list::print() {
	for (listNode* p = this->root; p !=0; p = p->next) {
		cout << p->data << " ";
	}
	cout << endl;
}

void list::reverse() {
	
	
	/* NOTE: we should do this WITHOUT allocating any memory.
	 * Just rearrange the arrows until the list is reversed.
	 * Put another way, memory footprint should be O(1). */
	 
	if(!root) return;

listNode *p =0;
listNode *q = root;
listNode *r = q->next;

while(r) 
{
	q->next = p;
	p = q;
	q = r;
	r = r->next;
}

q->next = p;
root = q;
} 
