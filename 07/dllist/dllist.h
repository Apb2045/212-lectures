#pragma once

struct dllistNode {
	dllistNode(dllistNode* p=0, int d = 0, dllistNode* n = 0): prev(p),data(d),next(n) {}
	dllistNode* prev;
	int data;
	dllistNode* next;
};

class dllist {
public:
	dllist(); /* constructor */
	/* since we have dynamic allocations now, need these too: */
	dllist(const dllist& L); /* copy constructor */
	~dllist(); /* destructor */
	dllist& operator=(const dllist& L);

	void insertAtFront(int x);
	void append(int x);
	void clear();
	void print();
	void printReverse();
	void reverse();
private:
	dllistNode* root;
	dllistNode* tail;
};

/* TODO: One potential criticism of the doubly linked list is that it will
 * take substantially more space than a singly linked list.  If you really
 * want a challenge, try to implement a doubly linked list *using the original
 * listNode structure*.  This is tricky, but it can be done.  NOTE: you should
 * be able to traverse the list in both directions (e.g., with print and
 * printReverse) with O(1) space overhead.  This this rules out linear stack
 * space, or making copies of the list, etc.  Hint: use algebra.
 * */
