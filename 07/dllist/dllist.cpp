#include "dllist.h"
#include <iostream>
using std::cout;
using std::endl;

dllist::dllist() {
	root = tail = 0;
}

dllist::dllist(const dllist& L) {
	/* TODO: write this */
}

dllist& dllist::operator=(const dllist& L) {
	/* TODO: write this */
	return *this;
}

dllist::~dllist() {
	this->clear();
}

void dllist::insertAtFront(int x) {
	this->root = new dllistNode(0,x,this->root);
	if (this->tail)
		this->root->next->prev = this->root;
	else /* list was empty; set tail to root */
		this->tail = this->root;
}
void dllist::append(int x) {
	/* TODO: write this. */
}
void dllist::clear() {
	/* abuse tail to make code shorter: */
	while ((tail = root)) {
		root = root->next;
		delete tail;
	}
}
void dllist::print() {
	for(dllistNode* p = root; p!=0; p = p->next)
		cout << p->data << " ";
	cout << endl;
}
void dllist::printReverse() {
	for(dllistNode* p = tail; p!=0; p = p->prev)
		cout << p->data << " ";
	cout << endl;
}
void dllist::reverse() {
	/* TODO: write this.  Should be easier than the singly
	 * linked list version. */
}
