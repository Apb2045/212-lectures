#include "list.h"
#include <iostream>
using std::cout;

void fn(list T) {
	cout << "copied list:\n";
	T.print();
}

int main(void)
{
	list L;
	for (int i = 0; i < 5; i++) {
		L.insertAtFront(i);
	}
	
	L.print();
	fn(L);
	cout << "original list:\n";
	L.print();
	L.reverse();
	L.print();
	return 0;
}
