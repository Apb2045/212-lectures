#/include "stack.h"
#include <stddef.h> /* for size_t */

stack::stack() {
	/* have to establish the class invariant.
	 * class invariant:
	 * 1. data points to an array of allocated elements.
	 * 2. size is the size of the stack, and data[0...size-1]
	 *    are the contents of the stack.
	 * 3. i < j < size implies that data[i] has been in the
	 *    stack longer than data[j].
	 * */
	this->allocated = 5; /* NOTE: 5 is pretty arbitrary */
	this->data = new int[allocated];
	this->size = 0;
}

stack::stack(const stack& S) :
	size(S.size), allocated(S.allocated)
{
	// this->data = S.data;  /* no good!! */
	/* the above might look sensible, but it goes horribly wrong x_x
	 * TODO: find out why.  Hint: try to pass a by value
	 * stack parameter to a function. */
	this->allocated = S.allocated;
	this->data = new int[this->allocated];
	this->size = S.size;
	for (size_t i = 0; i < this->size; i++) {
		this->data[i] = S.data[i];
	}
}

stack::~stack() {
	delete[] data;
}

/* TODO: try to write the assignment operator. */
/* TODO: try to write down push, pop, and top */
