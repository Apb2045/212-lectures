pragma once
#include <stddef.h> /* for size_t */

/* NOTE: we'll ignore things like error checking
 * for empty / full stack, etc...  */
class stack {
public:
	stack();
	stack(const stack& S);
	/* above line is the copy constructor.
	 * it's job is to (a) define what it means
	 * to pass a stack by value, and (b) what it
	 * means to make copies in general, say for
	 * temporary / intermediate computations
	 * or return values. */
	~stack();
	stack& operator=(const stack& RHS);
	void push(int x);
	int pop();
	int top();
private:
	int* data;
	size_t size;
	size_t allocated;
};

