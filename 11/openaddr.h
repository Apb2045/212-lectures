#pragma once
#include <inttypes.h>
#include <stddef.h>

/* hash table using plain open addressing.
 * Table stores integer values */

/* we'll just use a fixed hash function.  x is the value
 * to be hashed, and m is the range. */
size_t h(int x, size_t m);

class hashTable {
public:
	/* since we're using dynamic memory, we need:
	 * 1. constructor & copy constructor
	 * 2. destructor
	 * 3. assignment operator. */
	hashTable(size_t n = 20); /* n will be the size */
	hashTable(const hashTable& table);
	void insert(int x);
	bool search(int x);

private:
	bool* B; /* keeps track of whether or not each
				table entry is occupied. */
	int* T; /* hold the table of integers. */
	size_t size; /* size of both B and T */
};
