#pragma once
#include <stddef.h> /* for size_t */

/* comment this line out to disable copy constructor,
 * and crash main program. */
#define USECOPYCONST

/* NOTE: we'll ignore things like error checking
 * for empty / full stack, etc...  */
class stack {
public:
	stack();
	#ifdef USECOPYCONST
	stack(const stack& S);
	#endif
	/* above line is the copy constructor.
	 * it's job is to (a) define what it means
	 * to pass a stack by value, and (b) what it
	 * means to make copies in general, say for
	 * temporary / intermediate computations
	 * or return values. */
	~stack();
	stack& operator=(const stack& RHS);
	void push(int x);
	int pop();
	int top();
private:
	int* data; /* storage for stack elements */
	size_t size; /* current size of stack. */
	size_t allocated; /* the capacity */
};

